import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EmployeeDetail } from "../../app/core/user/model/employeedetail.model";
import { Employee, DetailEmployeeService, EmployeeService } from './employee.service';

@Component({
    selector: 'page-employee',
    templateUrl: 'employee.html',
    providers: [{provide:  EmployeeService, useClass: DetailEmployeeService}]
})

export class EmployeePage implements OnInit{
    employees: Employee[];

    // constructor(public navCtrl: NavController, public employee: DetailEmployeeService) {
    // }
    constructor(public navCtrl: NavController, public employee: EmployeeDetail) {
        this.employee.name = "ashwani";
    }

    ngOnInit(){
       // this.employees = this.employee.getEmployeeDetails();
       // console.log(this.employees);
    }
}