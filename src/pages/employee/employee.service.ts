import { Injectable } from "@angular/core";

export interface Employee{
    title: string;
    description: string;
    done: boolean;
}

@Injectable()
export abstract class EmployeeService {
  abstract getEmployeeDetails(): Employee[];
}

@Injectable()
export class DetailEmployeeService implements EmployeeService
{
    getEmployeeDetails(): Employee[]{
        const employees: Employee[] = [
            {
              title: 'get groceries',
              description: 'eggs, milk, etc.',
              done: false
            }
          ];
          return employees;
   }
}