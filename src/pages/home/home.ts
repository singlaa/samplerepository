import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { environment as ENV } from '../../environments/environment';
import { EmployeePage } from '../employee/employee';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  constructor(public navCtrl: NavController){
    console.log(ENV.BASE_URL);
  }

    loadEmployeePage(){
      this.navCtrl.push(EmployeePage);
    }
}


// export function compute(number) {
//   if (number < 0)
//     return number;

//   return number + 1;  
// }
